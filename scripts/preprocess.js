#!/usr/bin/env node
'use strict';

var pp = require('preprocess'),
	cb = function(err) {
			if(err) console.error(err);
	};

pp.preprocessFile('build/index.html', 'build/index.html', process.env, cb);
pp.preprocessFile('build/impressum.html', 'build/impressum.html', process.env, cb);
pp.preprocessFile('build/history.html', 'build/history.html', process.env, cb);

